1. install conda on macOS

    https://repo.anaconda.com/archive/Anaconda3-2020.11-MacOSX-x86_64.pkg

    Test:
      python --version 
        Python 3.8.8
    
      conda --version
        conda 4.9.2

2. create/list/remove environments

    conda create --name <env>
    conda env list
    conda env remove -n <env>

3. change to one environment

    conda activate <env>

4. List packages installed in the activated environment

    conda list

5. installing a package inside an environment

    conda install [-c conda-forge] <package>
    ie:
    conda install -y numpy
    conda install -y matplotlib
    conda install -y scipy

6. Update conda
    
    conda update -n base -c defaults conda

7. List available packages 

    firefox https://anaconda.org/anaconda

8. Clone an environment

    conda create --name new-env --clone from-venv 

9. Change python version for an environment

    conda activate env_xyz
    conda install python=3.7.7



References:

Anaconda management
[1] https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html

Anaconda vs pip/env
[2] https://docs.conda.io/projects/conda/en/latest/commands.html#conda-vs-pip-vs-virtualenv-commands

Search packages
[3] https://anaconda.org/anaconda
