Exercise pad as in the interviews
https://www.w3schools.com

Needs subscription:
https://leetcode.com/problemset/top-interview-questions/

https://docs.python.org/3/tutorial/index.html
https://docs.python.org/3/howto/sorting.html

https://pypi.org/project/pyserial
conda install -y --channel anaconda pyserial

(de)serialization 
https://docs.python.org/3.4/library/struct.html

inter-thread communication, thread-safe queues, locks
https://www.geeksforgeeks.org/python-communicating-between-threads-set-1/

matplotlib
https://www.youtube.com/watch?v=Ercd-Ip5PfQ
https://brushingupscience.com/2016/06/21/matplotlib-animations-the-easy-way/

Qt
https://www.youtube.com/watch?v=c8xMLtfUHTE

** make sure serial package may import from codec package
export PYTHONPATH=~/PycharmProjects/python-exercises 

flask
https://flask.palletsprojects.com/en/stable/
