import scipy.signal

if __name__ == "__main__":
    r, p, k = scipy.signal.residue([1, -1, 2, 0], [1, 0, -3, 2])
    print(r, p, k)

    r, p, k = scipy.signal.residue([1, -1, 2], [1, 0, -3, 2])
    print(r, p, k)