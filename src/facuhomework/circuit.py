from lcapy import Circuit
from matplotlib import pyplot as plt


def high_pass():
    cct = Circuit()
    cct.add("""
    Vi 1 0_1; down,
    C 1 2; right,
    R 2 0; down,
    W 0_1 0; right,
    W 0 0_2; right,
    P1 2_2 0_2; down,
    W 2 2_2;right,""")
    cct.draw(style='american', scale=1.0)
    cct.draw('highpass_circuit.png')


def low_pass():
    cct = Circuit()
    cct.add("""
    Vi 1 0_1; down,
    R 1 2; right,
    C 2 0; down,
    W 0_1 0; right
    W 0 0_2; right,
    P1 2_2 0_2; down
    W 2 2_2;right""")
    cct.draw(style='american', scale=1.0)
    cct.draw('lowpass_circuit.png')


def main():
    high_pass()
    #low_pass()


if __name__ == "__main__":
    main()
