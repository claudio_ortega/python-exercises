import numpy as np


def test1():
    ones = np.ones(3, float)
    twos = np.copy(ones)
    twos += ones
    half = twos / 4
    print(f'ones: {ones}')
    print(f'twos: {twos}')
    print(f'half: {half}')


def test2():
    size = 3
    a = np.zeros(size, float)
    b = np.ones(size, float)
    a += b
    a /= 4
    print(f'a: {a}')
    print(f'b: {b}')
    print(f'len(b): {len(b)}')


if __name__ == '__main__':
    test1()
    test2()

