import matplotlib.pyplot as plt
import numpy as np


def main():

    D1 = np.random.normal(0, 1, size=(3, 500))

    D2 = np.random.normal(0, 1, size=(50, 500))
    D2[10, :] = np.zeros((1, 500))

    D3 = np.random.normal(0, 1, size=(50, 500))

    D4 = np.linspace(0.0, 1.0, 10)
    D5 = np.linspace(1.0, 2.0, 10)
    D6 = np.vstack((D4, D5))

    # plot:
    fig, (ax1, ax2, ax3, ax4) = plt.subplots(1, 4)

    ax1.eventplot(D1, orientation="vertical", linewidth=0.5)
    ax2.eventplot(D2, orientation="vertical", linewidth=0.5)
    ax3.eventplot(D3, orientation="horizontal", linewidth=0.5)
    ax4.eventplot(D6, orientation="vertical", linewidth=0.5)

    plt.show()


if __name__ == "__main__":
    main()
