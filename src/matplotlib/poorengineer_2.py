#!/usr/bin/env python

import matplotlib.pyplot as plt
import matplotlib.animation as animation
import math


class TimeGraph3:
    def __init__(self, axis):

        self.t_cycle = 1024
        self.t_current = 0

        axis.grid(color="grey")

        axis.set_xlim(0, self.t_cycle)
        axis.set_ylim(-1, 1)

        self.line_value_text_t = axis.text(0.90, 0.90, 'fill-in-t', transform=axis.transAxes, fontsize='small', fontfamily='monospace')
        self.line_value_text_x = axis.text(0.90, 0.80, 'fill-in-x', transform=axis.transAxes, fontsize='small', fontfamily='monospace')
        self.line_value_text_y = axis.text(0.90, 0.70, 'fill-in-y', transform=axis.transAxes, fontsize='small', fontfamily='monospace')
        self.line_value_text_z = axis.text(0.90, 0.60, 'fill-in-z', transform=axis.transAxes, fontsize='small', fontfamily='monospace')

        self.line_label_t = 'time'
        self.line_label_x = 'X'
        self.line_label_y = 'Y'
        self.line_label_z = 'Z'
        self.style_x = 'c'
        self.style_y = 'r'
        self.style_z = 'b'

        self.line_x = axis.plot([], [], self.style_x, label=self.line_label_x)[0]
        self.line_y = axis.plot([], [], self.style_y, label=self.line_label_y)[0]
        self.line_z = axis.plot([], [], self.style_z, label=self.line_label_z)[0]

        self.t_list = []
        self.x_list = []
        self.y_list = []
        self.z_list = []

    def update_data(self):

        for i in range(0, 10):

            self.t_current = self.t_current + 1

            if self.t_current % self.t_cycle == 0:
                self.t_list = []
                self.x_list = []
                self.y_list = []
                self.z_list = []

            value_t = self.t_current % self.t_cycle
            value_x = math.sin(self.t_current * 0.03)
            value_y = value_x * 0.5
            value_z = value_x * 0.75

            self.line_value_text_t.set_text('t={0:.1f}'.format(self.t_current))
            self.line_value_text_x.set_text('x={0:+.4f}'.format(value_x))
            self.line_value_text_y.set_text('y={0:+.4f}'.format(value_y))
            self.line_value_text_z.set_text('z={0:+.4f}'.format(value_z))

            self.t_list.append(value_t)
            self.x_list.append(value_x)
            self.y_list.append(value_y)
            self.z_list.append(value_z)

        # x(t) line
        self.line_x.set_xdata(self.t_list)
        self.line_x.set_ydata(self.x_list)

        # y(t) line
        self.line_y.set_xdata(self.t_list)
        self.line_y.set_ydata(self.y_list)

        # z(t) line
        self.line_z.set_xdata(self.t_list)
        self.line_z.set_ydata(self.z_list)


def update_data(frame, state_a, state_b, state_c: TimeGraph3):
    state_a.update_data()
    state_b.update_data()
    state_c.update_data()


def main():

    fig = plt.figure(figsize=(12, 16))
    axes = fig.subplots(nrows=3, ncols=1, subplot_kw={'facecolor': "#d0d0d0"})

    tg_a = TimeGraph3(axes[0])
    tg_b = TimeGraph3(axes[1])
    tg_c = TimeGraph3(axes[2])

    anim = animation.FuncAnimation(
        fig,
        update_data,
        fargs=(tg_a, tg_b, tg_c),
        interval=5)  # in ms

    plt.show()


if __name__ == '__main__':
    main()
