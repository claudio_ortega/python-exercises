#!/usr/bin/env python

import time
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import math


class State:
    def __init__(self):
        self.dataType = None
        self.data = []
        self.plotTimer = 0
        self.previousTimer = 0
        self.numPlots = 1
        self.plotMaxLength = 1000
        self.x_list = []
        self.y_list = []


def update_data(frame, lines, line_value_text, line_label, time_text, state: State):

    current_timer = time.perf_counter()
    state.plotTimer = int((current_timer - state.previousTimer) * 1000)  # the first reading will be erroneous
    state.previousTimer = current_timer
    time_text.set_text('Plot Interval = ' + str(state.plotTimer) + 'ms')

    if frame % state.plotMaxLength == 0:
        state.x_list = []
        state.y_list = []

    for i in range(state.numPlots):
        state.x_list.append(frame % state.plotMaxLength)
        value = math.sin(frame * 0.01)
        state.y_list.append(value)
        lines[i].set_xdata(state.x_list)
        lines[i].set_ydata(state.y_list)
        line_value_text[i].set_text('[' + line_label[i] + '] = ' + str(value))


def main():

    state = State()

    x_min = 0
    x_max = state.plotMaxLength
    y_min = -1
    y_max = 1

    fig = plt.figure(figsize=(10, 8))

    ax = plt.axes(
        xlim=(x_min, x_max),
        ylim=(float(y_min - (y_max - y_min) / 10), float(y_max + (y_max - y_min) / 10)))

    ax.set_title('Accelerometer')
    ax.set_xlabel("Time")
    ax.set_ylabel("Accelerometer Output")

    line_labels = ['X', 'Y', 'Z']
    style = ['c', 'r', 'b']  # line styles for the different plots
    time_text = ax.text(0.70, 0.95, '', transform=ax.transAxes)
    lines = []
    line_value_text = []

    # init graphs
    for i in range(state.numPlots):
        lines.append(ax.plot([], [], style[i], label=line_labels[i])[0])
        line_value_text.append(ax.text(0.70, 0.90 - i * 0.05, '', transform=ax.transAxes))

    anim = animation.FuncAnimation(
        fig,
        update_data,
        fargs=(lines, line_value_text, line_labels, time_text, state),
        interval=5)  # in ms

    plt.show()


if __name__ == '__main__':
    main()
