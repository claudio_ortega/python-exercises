import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

TWO_PI = 2 * np.pi
TS = 0.1


def main():

    fig = plt.figure(figsize=(12, 16))
    axes = fig.subplots(nrows=3, ncols=3)

    t = np.arange(0.0, TWO_PI, TS)
    s = np.sin(2*t)
    axes[0, 0].plot(t, s)
    axes[1, 1].plot(t, 2*s, color="red")
    axes[1, 1].plot(2*t, 2*s, color="blue")
    axes[0, 1].plot(t, 0*s, color="green")

    axes[2, 2].plot(0, 0, color="green")

    red_dot_plot, = axes[0, 0].plot(0, 0, 'ro')
    green_dot_plot, = axes[0, 1].plot(0, 0, 'go')

    def animate1(i):
        red_dot_plot.set_data(i, np.sin(2*i))
        green_dot_plot.set_data(i, 0)

        axes[2, 2].clear()

        if (i/100) % 2 == 0:
            print("{0:s} -- {1:f}".format("    1", i))
            moving_plot_2, = axes[2, 2].plot([0, 1], [0, 1])
        else:
            print("{0:s} -- {1:f}".format("2", i))
            moving_plot_2, = axes[2, 2].plot([0, 1], [1, 0])

        return red_dot_plot, green_dot_plot, moving_plot_2,

    anim = animation.FuncAnimation(
        fig=fig,
        func=animate1,
        frames=np.arange(0.0, TWO_PI, TS),
        interval=50,  # mSec
        blit=True,
        repeat=True)

    plt.show()


if __name__ == "__main__":
    main()
