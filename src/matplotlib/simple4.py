import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

TWO_PI = 2 * np.pi


def main():

    fig, ax = plt.subplots()

    t = np.arange(0.0, TWO_PI, 0.001)
    s = np.sin(2*t)
    sin_plot = plt.plot(t, s)
    ax = plt.axis([0, TWO_PI, -1, 1])
    red_dot_plot, = plt.plot([], [], 'ro')

    def animate(i):
        print(i)
        red_dot_plot.set_data(i, np.sin(2*i))
        return red_dot_plot,

    # create animation using the animate() function

    global anim
    anim = animation.FuncAnimation(
        fig=fig,
        func=animate,
        frames=np.arange(0.0, TWO_PI, 0.1),
        interval=1000,
        blit=True,
        repeat=True)

    plt.show()


if __name__ == "__main__":
    main()
