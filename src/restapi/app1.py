from flask import Flask


# test:
#   curl localhost:8080/reload_config


def main():
    app = Flask("hello")

    @app.route('/reload_config', methods=['GET'])
    def get():
        return {"message": "KC gordo!"}, 200

    app.run(debug=True, host='0.0.0.0', port=8080)


if __name__ == "__main__":
    main()
