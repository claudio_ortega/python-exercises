import tkinter as tk
from tkinter import ttk
import time

# See https://www.pythontutorial.net/tkinter for learning tkinter
# See https://www.pythontutorial.net/tkinter/tkinter-after on how to use Tk.after for periodic updates


class DigitalClock(tk.Tk):
    def __init__(self):
        super().__init__()
        self.title('Digital Clock')
        self.geometry('500x150')
        self.style = ttk.Style(self)
        self.style.configure('TLabel', background='black', foreground='red')
        self.label = ttk.Label(self, text="", font=('monospace', 80))
        self.label.pack(expand=True)
        self.after(0, self.update)

    def update(self):
        self.label.configure(text=time.strftime('%H:%M:%S'))
        self.after(100, self.update)


if __name__ == "__main__":
    clock = DigitalClock()
    clock.mainloop()

