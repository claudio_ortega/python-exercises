import tkinter as tk
from tkinter import ttk
import threading
import time


class App(tk.Tk):
    def __init__(self):
        super().__init__()
        self.title("Threading Example")
        self.button = ttk.Button(self, text="Start A", command=self.start_task_a)
        self.button.pack(pady=20)
        self.button = ttk.Button(self, text="Start B", command=self.start_task_b)
        self.button.pack(pady=20)
        self.progress = ttk.Progressbar(self, orient="horizontal", length=200, mode="indeterminate")
        self.progress.pack(pady=20)

    def start_task_a(self):
        running_threads = threading.enumerate()
        print("A -- start non-daemon mode", [thread.name for thread in running_threads])
        if len(running_threads) > 1:
            print("sorry, system busy...")
            return
        threading.Thread(target=self.task_a, daemon=True).start()

    def start_task_b(self):
        running_threads = threading.enumerate()
        print("B -- start daemon mode", [thread.name for thread in running_threads])
        if len(running_threads) > 1:
            print("sorry, system busy...")
            return
        threading.Thread(target=self.task_b, daemon=True).start()

    def task_a(self):
        print("A -- BEGIN")
        self.after(0, self.progress.start)
        self.progress.start()
        time.sleep(2)
        self.after(0, self.progress.stop)
        print("A -- END")

    def task_b(self):
        print("B -- BEGIN")
        self.after(0, self.progress.start)
        time.sleep(2)
        self.after(0, self.progress.stop)
        print("B -- END")

    def quit(self):
        print("initiating quitting sequence")
        exit(0)


if __name__ == "__main__":
    app = App()
    app.protocol("WM_DELETE_WINDOW", app.quit)
    app.mainloop()


