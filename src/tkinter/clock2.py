import tkinter as tk
from tkinter import ttk
import time

# See https://www.pythontutorial.net/tkinter for learning tkinter
# See https://www.pythontutorial.net/tkinter/tkinter-after on how to use Tk.after for periodic updates

def update(label):
    label.configure(text=time.strftime('%H:%M:%S'))
    label.after(100, update, label)


def main():
    root = tk.Tk()
    root.title('Digital Clock')
    root.geometry('500x150')
    root.style = ttk.Style(root)
    root.style.configure('TLabel', background='black', foreground='red')
    root.label = ttk.Label(root, text="", font=('monospace', 80))
    root.label.pack(expand=True)
    root.label.after(0, update, root.label)
    root.mainloop()


if __name__ == "__main__":
    main()

