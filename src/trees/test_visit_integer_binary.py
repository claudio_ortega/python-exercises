from src.trees.binary_tree import BinaryTreeNode


if __name__ == '__main__':

    r = BinaryTreeNode(20, True)
    r.is_root = True

    a1 = BinaryTreeNode(10)
    a2 = BinaryTreeNode(30)

    r.left = a1
    r.right = a2

    a11 = BinaryTreeNode(5)
    a12 = BinaryTreeNode(15)

    a1.left = a11
    a1.right = a12

    def compare_ints(inta, intb: int):
        print("comparing nodes {0:d} {1:d}".format(inta, intb))
        if inta > intb:
            return 1
        if inta < intb:
            return -1
        else:
            return 0

    found = r.binary_search( 5, compare_ints )
    if found is None:
        print("not found")
    else :
        print("found {0:d} {1:d}".format(found.id, found.data))
