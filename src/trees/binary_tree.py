

class BinaryTreeNode:

    def __init__(self, data, root=False):
        self.data = data
        self.id = id(data)
        self.is_root = root
        self.left = None
        self.right = None

    # returns true to stop the traversal
    def visit_left_order(self, visitor):
        if self.left is not None:
            should_stop = self.left.visit_left_order(visitor)
            if should_stop:
                return True

        should_stop = visitor(self)
        if should_stop:
            return True

        if self.right is not None:
            should_stop = self.right.visit_left_order(visitor)
            if should_stop:
                return True

        return False

    """
        comparator: def ( int, int )
    """
    def binary_search(self, target, comparator):

        comparison = comparator(target, self.data)

        if comparison == 0:
            return self

        if comparison < 0 and self.left is not None:
            return self.left.binary_search(target, comparator)

        if comparison > 0 and self.right is not None:
            return self.right.binary_search(target, comparator)

        return None


class GraphCycleDetector:

    def __init__(self):
        self.set = set()

    # returns true if it detects a cycle
    # it detects cycles by checking if it visits any node more than once
    def check(self, node: BinaryTreeNode):
        def add_to_set(node: BinaryTreeNode):
            if node.id in self.set:
                print("cycle detected:", self.set, node.id)
                return True

            self.set.add(node.id)
            return False

        return node.visit_left_order(add_to_set)

