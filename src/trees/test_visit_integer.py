from src.trees.binary_tree import BinaryTreeNode, GraphCycleDetector


if __name__ == '__main__':

    r = BinaryTreeNode(20, True)
    r.is_root = True

    a1 = BinaryTreeNode(10)
    a2 = BinaryTreeNode(30)

    r.left = a1
    r.right = a2

    a11 = BinaryTreeNode(5)
    a12 = BinaryTreeNode(15)

    a1.left = a11
    a1.right = a12

    # create cycles
    #r.right = r
    #a11.right = a12

    detector = GraphCycleDetector()

    assert (not detector.check(r))


    def print_string_node(node: BinaryTreeNode):
        print("(a) {0:d} {1:d}".format(node.id, node.data))
        return False

    r.visit_left_order(print_string_node)

    target = 78

    def finder(node : BinaryTreeNode):
        print("(b) {0:d} {1:d}".format(node.id, node.data))
        return node.data == target

    r.visit_left_order(finder)
