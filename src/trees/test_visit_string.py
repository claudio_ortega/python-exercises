from src.trees.binary_tree import BinaryTreeNode, GraphCycleDetector


if __name__ == '__main__':
    r = BinaryTreeNode('root', True)
    r.is_root = True

    a1 = BinaryTreeNode('a.1')
    a2 = BinaryTreeNode('a.2')

    r.left = a1
    r.right = a2

    a11 = BinaryTreeNode('a.1.1')
    a12 = BinaryTreeNode('a.1.2')

    a1.left = a11
    a1.right = a12

    # create cycles
    #r.right = r
    #a11.right = a12

    detector = GraphCycleDetector()

    assert (not detector.check(r))


    def print_string_node(node: BinaryTreeNode):
        print("(a) {0:d} {1:s}".format(node.id, node.data))
        return False


    r.visit_left_order(print_string_node)

    target = "a.1.1"
    #target = "not-there"

    def finder(node : BinaryTreeNode):
        print("(b) {0:d} {1:s}".format(node.id, node.data))
        return node.data == target

    r.visit_left_order(finder)
