import json
import numpy as np
from sklearn.metrics import confusion_matrix, accuracy_score


def load_into_dict(results: dict, event: str, trial: str, neuron: str, psth):
    if event not in results:
        results[event] = dict()
    if trial not in results[event]:
        results[event][trial] = dict()
    results[event][trial][neuron] = psth


def trial_row(results: dict, event: str, trial_number: int):
    test_row = []
    for neuron in results[event][trial_number]:
        test_row = test_row + results[event][trial_number][neuron]
    return test_row


def compute_template(results: dict, event: str, event_exclude: str, trial_number_exclude: int):
    hist_sum = None
    for trial_number in results[event]:
        if trial_number == trial_number_exclude and event == event_exclude:
            continue
        tmp = np.array(trial_row(results, event, trial_number))
        if hist_sum is None:
            hist_sum = tmp
        else:
            hist_sum = hist_sum + tmp
    return hist_sum / len(results[event])


def compute_distance(mean_template: np.array, test_row: np.array):
    squared = np.square(mean_template - test_row)
    return np.sqrt(sum(squared))


def classifier(results: dict, event_to_exclude: str, trial_to_exclude: int):
    test_trial = trial_row(results, event_to_exclude, trial_to_exclude)
    minimum_distance = None
    minimum_event = "n/a"
    for event in results:
        event_template = compute_template(results, event, event_to_exclude, trial_to_exclude)
        distance = compute_distance(event_template, test_trial)
        if minimum_distance is None or distance < minimum_distance:
            minimum_distance = distance
            minimum_event = event
    return minimum_event


def exercise3(bin_size: float):
    hw3_results = dict()
    example_data = json.load(open('example_hw3.json'))

    for neuron, response_times in example_data['neurons'].items():
        np_response_times = np.array(response_times)
        for event, trial_times in example_data['events'].items():
            baseline_start = 0.0
            response_end = 0.2
            event_window = list(np.arange(baseline_start, response_end, bin_size))
            total_bins = len(event_window)
            bin_sum = np.zeros(total_bins)
            trial_count = 0
            for trial_time in trial_times:
                trial_count = trial_count + 1
                stim_to_resp_time = np_response_times - trial_time
                binned_spikes, bins = np.histogram(stim_to_resp_time, total_bins, range=(baseline_start, response_end))
                bin_sum = bin_sum + binned_spikes
                load_into_dict(hw3_results, event, trial_count, neuron, binned_spikes.tolist())

    true_values = []
    predicted_values = []
    for event in hw3_results:
        for trial_number in hw3_results[event]:
            classified_event = classifier(hw3_results, event, trial_number)
            print(f'event: {event}, trial_number:{trial_number}, classified:{classified_event}')
            true_values = true_values + [event]
            predicted_values = predicted_values + [classified_event]

    cm = confusion_matrix(true_values, predicted_values)
    print(f'cm: {cm}')

    acc_score = accuracy_score(true_values, predicted_values)
    print(f'acc_score: {acc_score}')

    out_file = open("Ortega_Facundo_hw3.json", "w")
    json.dump(hw3_results, out_file, indent=4)


if __name__ == '__main__':
    exercise3(0.01)

