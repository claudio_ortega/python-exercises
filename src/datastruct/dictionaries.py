import json


def load_into_dict(d: dict, k1: str, k2: str, k3: str, value):
    if k1 not in d:
        d[k1] = dict()
    if k2 not in d[k1]:
        d[k1][k2] = dict()
    d[k1][k2][k3] = value


def test():
    top_dict = dict()
    load_into_dict(top_dict, "level_a_1", "level_b_1", "a1", float(1))
    load_into_dict(top_dict, "level_a_1", "level_b_2", "a2", int(2))
    load_into_dict(top_dict, "level_a_2", "level_b_1", "a3", int(3))
    load_into_dict(top_dict, "level_a_2", "level_b_1", "a4", [1, 2, 3, 4, 5, 6, 7, 8, 9, 10])

    print(json.dumps(top_dict, indent=4))

    out_file = open("results.json", "w")
    json.dump(top_dict, out_file, indent=4)


if __name__ == '__main__':
    test()

