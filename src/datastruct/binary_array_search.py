
# if the passed value is in the array then it returns its index, otherwise returns -1
def binary_search(in_array, in_target_value) -> int:
    
    assert_list_is_sorted(in_array)
    
    if len(in_array) == 0:
        return -1

    # len > 0,
    # obtain first, median, and last index
    first_index = 0
    last_index = len(in_array)-1
    median_index = (first_index+last_index)//2

    if in_target_value < in_array[first_index]:
        return -1
    if in_target_value > in_array[last_index]:
        return -1
    if in_target_value == in_array[median_index]:
        return median_index

    if in_target_value > in_array[median_index]:
        tmp = binary_search(in_array[median_index + 1:last_index + 1], in_target_value)
        if tmp == -1:
            return -1
        return median_index+1 + tmp
    else:        # case: in_target_value < in_array[median]:
        return binary_search(in_array[first_index:median_index + 1], in_target_value)


def assert_list_is_sorted(in_array):
    in_array_copy = in_array.copy()
    in_array_copy.sort()
    assert in_array_copy == in_array


if __name__ == '__main__':

    #
    assert(binary_search([0, 4, 7, 10], 0) == 0)
    assert(binary_search([0, 4, 7, 10], 4) == 1)
    assert(binary_search([0, 4, 7, 10], 7) == 2)
    assert(binary_search([0, 4, 7, 10], 10) == 3)
    assert(binary_search([0, 4, 7, 10], 11) == -1)
    assert(binary_search([0, 4, 7, 10], -2) == -1)

    #
    assert(binary_search([0, 4], 0) == 0)
    assert(binary_search([0, 4], 4) == 1)

    #
    assert(binary_search([0], 0) == 0)
    assert(binary_search([0], 4) == -1)

    #
    assert(binary_search([], 0) == -1)
    assert(binary_search([], 4) == -1)

    print("done")

