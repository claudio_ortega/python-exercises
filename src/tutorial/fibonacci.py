def fib(n) -> None:
    a, b = 0, 1
    while a < n:
        print(a, end=' ')
        a, b = b, a+b
    print()


def main():
    fib(100)


if __name__ == "__main__":
    main()
