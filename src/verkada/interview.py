import threading
import time
from enum import Enum


class Status(Enum):
    Valid: int = 1
    NoChange: int = 2
    Error: int = 3


class SensorReading:
    def __init__(self, _data, _time, _status):
        self.data = _data
        self.time = _time
        self.Status = _status


"""
    test sequence:
    1 - wrong measurement arrives
    2 - good measurement arrives
    3 - good measurement arrives, change
    4 - good measurement arrives, no change
    >= 5 - error measurement arrives
"""


class Sensor:

    def __init__(self):
        self.latestGoodReading = None
        self.thread = None
        self.test_sequence = 1
        self.stopped = True

    def blocking(self) -> SensorReading:

        max_delay = 5

        if self.test_sequence == 1:
            time.sleep(1)
            sensor_reading = SensorReading(0.0, 0, Status.Error)

        elif self.test_sequence == 2:
            time.sleep(2)
            sensor_reading = SensorReading(10.0, 0, Status.Valid)

        elif self.test_sequence == 3:
            time.sleep(3)
            sensor_reading = SensorReading(11.0, 0, Status.Valid)

        elif self.test_sequence == 4:
            time.sleep(max_delay)
            sensor_reading = SensorReading(0, 0, Status.NoChange)

        else:
            time.sleep(2)
            sensor_reading = SensorReading(0.0, 0, Status.Error)

        self.test_sequence += 1

        return sensor_reading

    def task_1(self):
        while not self.stopped:
            newReading = self.blocking()
            print("blocking returns:{0:f} {1:s}".format(newReading.data, newReading.Status))
            if newReading.Status == Status.Valid:
                self.latestGoodReading = newReading

    def start(self):
        self.stopped = False
        self.thread = threading.Thread(target=self.task_1)
        self.thread.start()

    def stop(self):
        self.stopped = True

    def non_blocking(self) -> SensorReading:
        return self.latestGoodReading


if __name__ == "__main__":

    sensor = Sensor()
    sensor.start()

    time.sleep(0.5)

    for i in range(8):
        reading = sensor.non_blocking()
        if reading is None:
            print("no measurement yet")
        else:
            print("non blocking returns:{0:f} {1:s}".format(reading.data, reading.Status))
        time.sleep(1)

    sensor.stop()
    print("waiting")
    time.sleep(5)
    print("done")
