import inspect
import pickle
import struct

"""
   this file attempts to capture a poc for the monitoring of the Thermo Gadget(R).
"""

"""
    code producing the array of bytes as of today in the teensy
    
    NOTE: 
    this code was originally NOT sending the numActiveZones along,
    but it was modified to do so.
    
    bool Serial_IO::writeZoneData(
        StateControl& _sc, 
        Melexis_90640 _mlx, 
        float* temperature_data, 
        uint16_t numActiveZones) {
    
        writeBufferCode(0xFE ); // 0xFE signals start of zone frame
        writeFourCodedBytes(_sc.globalTime_ms );
        writeFourCodedBytes(_sc.stateTime_ms );
        writeFourCodedBytes(_sc.globalCounter );
        writeFourCodedBytes(_sc.stateCounter );
        
        // this is new
        writeFourCodedBytes(numActiveZones);
        
        for (uint16_t ii = 0; ii < numActiveZones; ii++) {
            writeTwoCodedBytes((uint16_t)(temperature_data[ii]*100) ); // Write temperatures
        }
        writeTwoCodedBytes((uint16_t)(_sc.setPoint*100) ); // Write set point data
        for (uint16_t ii = 0; ii < numActiveZones; ii++) {
            writeTwoCodedBytes((uint16_t)(_sc.pwmArrayLast[ii]*100) ); // Write PWM levels, pwmArrayLast is updated when output is written
        }
        writeTwoCodedBytes((uint16_t)(_sc.fanPWMLast*100) ); // Write fan state (on = 100), fanStateLast is updated when output is written
        writeTwoCodedBytes((uint16_t)(100*_sc.stateStageCounter+_sc.state) ); // Write state enum and counter
        uint16_t bitData = 0;
        for (uint16_t ii = 0; ii < numActiveZones; ii++) { // Left shift bits to populate bitData
            bitData += (_sc.advanceState[ii])<<ii;
        }
        bitData += _mlx.pageState<<(numActiveZones+1);
        writeTwoCodedBytes((uint16_t)bitData ); // Write bit data on advancement bits and image page data
        for (uint16_t ii = 0; ii < numActiveZones; ii++) {
            // writeTwoCodedBytes((uint16_t)(_sc.temperatureNext[ii]*100) ); // Write calculated temperatures
            writeTwoCodedBytes((uint16_t)(_sc.driveTermDebug[ii]*100) ); // Write interim drive terms (ramp, high/low terms)
        }
        // writeTwoCodedBytes((uint16_t)(100.0) ); // Write Melexis temperature
        writeTwoCodedBytes((uint16_t)(_mlx.TaLatest*100) ); // Write Melexis temperature
        writeTwoCodedBytes((uint16_t)(_sc.lastTempMeasurements[0]*100) ); // Write fan temperature
        writeTwoCodedBytes((uint16_t)(_sc.lastTempMeasurements[1]*100) ); // Write LED 1 temperature
        writeTwoCodedBytes((uint16_t)(_sc.lastCurrentMeasurement*100) ); // Write total current
        
        return true;
    }
"""

"""
    This structure/class conveniently encapsulates all and ONLY the data that is being sent
    by the teensy and received by the python process.
    
    It is NOT expected to correlate exactly with a structure in C, rather with the data being sent
    strictly, nor more not less. IOW, do not not try to use this for anything else. 
"""


class TypeFormatDef:
    def __init__(self, size, scale):
        self.size = size
        self.scale = scale


class Constants:
    INTEGER_PACK_FORMAT = "< H"
    INTEGER_PACK_LENGTH = 2
    TYPE_FORMAT = {
        'globalTime_ms': TypeFormatDef(2, 1),
        'numActiveZones': TypeFormatDef(2, 1),
        'temperatureData': TypeFormatDef(2, 1),
    }


class ThermoCyclerData:
    def __init__(self):
        self.globalTime_ms = None
        self.numActiveZones = None
        self.temperatureData = None

    def fill_in_dataset_testa(self):
        self.globalTime_ms = 10
        self.numActiveZones = 2
        self.temperatureData = [3, 4]
        return self


"""
    serialize_thermo_gadget_output simulates the output produced by the code above,
    that even written in a different language produces a predictable format. 
    
    it needs more work, ie: you need to add all the other variables you are sending over,
    but is a trivial repetition of the same idea, nothing can go wrong.
"""


def serialize_thermo_gadget_output(t: ThermoCyclerData) -> bytes:
    tx_payload = struct.pack(Constants.INTEGER_PACK_FORMAT, t.globalTime_ms)
    tx_payload = tx_payload + struct.pack(Constants.INTEGER_PACK_FORMAT, t.numActiveZones)
    for i in range(0, t.numActiveZones):
        tx_payload = tx_payload + struct.pack(Constants.INTEGER_PACK_FORMAT, t.temperatureData[i])
    return tx_payload


def deserialize_thermo_gadget_output(b: bytes) -> ThermoCyclerData:
    r = ThermoCyclerData()
    current_byte = 0

    r.globalTime_ms = struct.unpack(
        Constants.INTEGER_PACK_FORMAT,
        b[current_byte:current_byte + Constants.INTEGER_PACK_LENGTH])[0]
    type_format = Constants.TYPE_FORMAT["globalTime_ms"]
    current_byte += type_format.size

    r.numActiveZones = struct.unpack(
        Constants.INTEGER_PACK_FORMAT,
        b[current_byte:current_byte + Constants.INTEGER_PACK_LENGTH])[0]
    type_format = Constants.TYPE_FORMAT["numActiveZones"]
    current_byte += type_format.size

    r.temperatureData = []
    for i in range(0, r.numActiveZones):
        temp = struct.unpack(
            Constants.INTEGER_PACK_FORMAT,
            b[current_byte:current_byte + Constants.INTEGER_PACK_LENGTH])[0]
        r.temperatureData = r.temperatureData + [temp]
        type_format = Constants.TYPE_FORMAT["temperatureData"]
        current_byte += type_format.size

    return r


def test():
    # simulates the data being sent by the Teensy
    t = ThermoCyclerData().fill_in_dataset_testa()
    tx_payload = serialize_thermo_gadget_output(t)
    print("tx_payload -- {0:d} -- {1:s}".format(len(tx_payload), str(list(tx_payload))))

    # POC showing that it is possible and simple to deserialize using just the struct package
    # without any use of reflection or conditionals.
    r = deserialize_thermo_gadget_output(tx_payload)

    # verify that both sides (teensy,python) have the same data
    assert pickle.dumps(t) == pickle.dumps(r)


if __name__ == '__main__':
    test()
