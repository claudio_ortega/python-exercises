import threading
import socket
import time


def task_client():
    udp_client_socket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
    while True:
        udp_client_socket.sendto(str.encode("hello from client"), ("192.168.1.98", 20001))
        msg_from_server = udp_client_socket.recvfrom(1024)
        msg = "client: message back from server: {}".format(msg_from_server[0])
        print(msg)
        time.sleep(1)


def task_server():
    udp_server_socket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
    udp_server_socket.bind(("192.168.1.98", 20001))
    print("UDP server up and listening")
    while True:
        bytes_address_pair = udp_server_socket.recvfrom(1024)
        message = bytes_address_pair[0]
        address = bytes_address_pair[1]
        print("server: from:{}, msg:{}".format(address, message))

        # send a reply to the client
        udp_server_socket.sendto(str.encode("hello from server"), address)


def main():
    print("start")

    t_p = threading.Thread(target=task_client, args=())
    t_c = threading.Thread(target=task_server, args=())

    t_c.start()
    t_p.start()

    t_c.join()
    t_p.join()

    print("stop")


if __name__ == "__main__":
    main()


