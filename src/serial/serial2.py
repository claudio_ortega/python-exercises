import sys
import threading
import serial
import struct
import platform
import signal
import time
from codec import Codec


def tx_loop(
        kill_switch: threading.Event,
        serial_port: serial.Serial,
        pack_format: str,
        max_loops: int,
        ):

    loop = 0
    close_detected = False
    while (max_loops < 0 or loop < max_loops) and not close_detected:
        try:
            if kill_switch.is_set():
                print("quitting tx loop (0), kill switch detected")
                close_detected = True

            payload = struct.pack(
                pack_format,
                loop, 1, 2, 3, 4, 5, 6)

            tx_frame = Codec.encode(payload)

            serial_port.write(tx_frame)
            loop = loop + 1

        except serial.SerialException as e:
            print("quitting tx loop (1) - " + str(e))
            break

    print("quitting tx loop")


def rx_loop(
        kill_switch: threading.Event,
        serial_port: serial.Serial,
        pack_format: str,
        ):
    c = Codec()
    data_rx_ok_count = 0
    close_detected = False
    while not close_detected:
        try:
            if kill_switch.is_set():
                print("quitting rx loop (0), kill switch detected")
                close_detected = True

            rx_bytes = serial_port.read(size=1)

            if len(rx_bytes) == 0:
                # print("quitting rx loop (1), timeout detected")
                pass

            for byte in rx_bytes:
                # print("rx loop - byte -- {0:2x}".format(byte))
                (rx_payload, rx_frame, frame_detected) = c.decode(byte)
                if frame_detected:
                    data_rx_ok_count = data_rx_ok_count + 1
                    data = struct.unpack(pack_format, rx_payload)
                    print("rx loop - data -- {:10d} -- {:s}".format(data_rx_ok_count, str(list(data))))

        except serial.SerialException as e:
            print("quitting rx loop (2) - " + str(e))
            close_detected = True

        except TypeError as e:
            print("quitting rx loop (3) - " + str(e))
            close_detected = True

    print("quitting rx_loop")


def idle_loop(kill_switch: threading.Event):
    close_detected = False
    count = 0
    while not close_detected:
        try:
            if kill_switch.is_set():
                print("quitting idle loop (0), kill switch detected")
                close_detected = True

            count += 1
            print(f'idle loop, count:{count}, threads():{threading.enumerate()}')

            time.sleep(5)
            print("idle loop, out from sleep")

        except Exception as e:
            print("quitting idle loop (1) - " + str(e))

    print("quitting idle_loop")


def test():
    try:
        thread_kill_switch = threading.Event()

        def receive_signal(signal_number, _):
            print("received signal {}".format(signal_number))
            thread_kill_switch.set()
            sys.exit()

        signal.signal(signal.SIGINT, receive_signal)

        serial_conn = serial.Serial()
        serial_conn.baudrate = 115200
        serial_conn.timeout = 0.5
        serial_conn.bytesize = serial.EIGHTBITS
        serial_conn.stopbits = serial.STOPBITS_ONE
        serial_conn.parity = serial.PARITY_NONE

        if platform.system() == "Linux":
            serial_conn.port = "/dev/ttyUSB0"
        else:
            serial_conn.port = "/dev/tty.usbserial-AO0099I5"

        serial_conn.open()
        print("connected on {} at {}".format(serial_conn.port, serial_conn.baudrate))

        tx_max_loops = -1
        # pack_format = '<I ffffff'
        pack_format = '< I BBBBBB'

        tx_thread = threading.Thread(
            name="tx_thread",
            daemon=True,
            target=tx_loop,
            args=(thread_kill_switch, serial_conn, pack_format, tx_max_loops))
        rx_thread = threading.Thread(
            name="rx_thread",
            daemon=True,
            target=rx_loop,
            args=(thread_kill_switch, serial_conn, pack_format))
        idle_thread = threading.Thread(
            name="idle_thread",
            daemon=True,
            target=idle_loop,
            args=(thread_kill_switch, ))

        tx_thread.start()
        rx_thread.start()
        idle_thread.start()

        print("started all threads")

        tx_thread.join()
        print("tx thread exited")

        rx_thread.join()
        print("rx thread exited")

        idle_thread.join()
        print("idle thread exited")

        serial_conn.close()
        print("quitting main")

    except serial.SerialException as e:
        print("failed to connect: " + str(e))

    except Exception as e:
        print("exception: " + str(e))
        sys.exit()


if __name__ == '__main__':
    test()

