import binascii
import struct


class Codec:
    # stateless & constants
    header_length = 6
    checksum_length = 4
    header_byte_filler = 0x55
    max_payload_length = 256

    # stateless functions
    def encode(payload: bytes) -> bytes:

        if type(payload) is not bytes:
            raise Exception('wrong type for payload: {}'.format(type(payload)))

        if len(payload) > Codec.max_payload_length:
            raise Exception('len(payload) is: {}, should not be more than: {}'.format(len(payload), Codec.max_payload_length))

        checksum = compute_checksum(payload)
        checksum_bytes = struct.pack("I", checksum)

        if len(checksum_bytes) != Codec.checksum_length:
            raise Exception(
                'len(checksum_bytes) is: {}, should be: {}'.format(len(checksum_bytes), Codec.checksum_length))

        length_bytes = list(struct.pack("h", len(payload)))

        encoded = [Codec.header_byte_filler] * Codec.header_length + length_bytes + list(payload) + list(checksum_bytes)

        return bytes(encoded)

    # stateful stuff
    def __init__(self):
        self.payload_length = -1
        self.bytes_received = []
        self.time_index = 0

    """
        payload is of type bytes
        return value: list of bytes with header and checksum
    """

    def decode(self, byte) -> (bytes, bytes, bool):

        # at the header
        if len(self.bytes_received) < Codec.header_length:
            if byte != self.header_byte_filler:
                self.bytes_received = []
            else:
                self.bytes_received.append(byte)
            return bytes([]), bytes([]), False

        # at the payload size value (2 bytes)
        if len(self.bytes_received) < (Codec.header_length + 2):
            self.bytes_received.append(byte)
            if len(self.bytes_received) != Codec.header_length + 2:
                return bytes([]), bytes([]), False
            else:
                l1 = self.bytes_received[Codec.header_length:Codec.header_length+2]
                payload_size_bytes = bytes(l1)
                t1 = struct.unpack("h", payload_size_bytes)
                self.payload_length = t1[0]
                return bytes([]), bytes([]), False

        # at the payload
        if len(self.bytes_received) < (Codec.header_length + self.payload_length + 2):
            self.bytes_received.append(byte)
            return bytes([]), bytes([]), False

        # at the checksum
        if len(self.bytes_received) < (Codec.header_length + self.payload_length + Codec.checksum_length + 2):
            self.bytes_received.append(byte)

            # last byte in the frame?
            if len(self.bytes_received) < (Codec.header_length + self.payload_length + Codec.checksum_length + 2):
                return bytes([]), bytes([]), False
            else:
                # yes, last byte in the frame
                payload = self.bytes_received[Codec.header_length + 2:Codec.header_length + self.payload_length + 2]
                checksum_payload_int = compute_checksum(bytes(payload))
                checksum_rcv_bytes = bytes(self.bytes_received[Codec.header_length + self.payload_length + 2:])
                checksum_rcv_int = struct.unpack("I", checksum_rcv_bytes)

                if checksum_payload_int == checksum_rcv_int[0]:
                    # checksum is OK
                    self.time_index = self.time_index + 1
                    ret_tuple = bytes(payload), bytes(self.bytes_received), True
                    self.bytes_received = []
                    return ret_tuple
                else:
                    print("bad crc, len:{:d}, rcv:{:d}, computed:{:d}".format(
                        len(self.bytes_received),
                        checksum_payload_int,
                        checksum_rcv_int[0]))
                    return bytes([]), bytes([]), False

        self.bytes_received = []
        return bytes([]), bytes([]), False


def compute_checksum(payload: bytes) -> int:
    r = binascii.crc32(payload) & 0xffffffff
    return r


def test():
    # simulate 6x (x,y,z) measurements plus a time index
    tx_payload = struct.pack(
        '<I fff fff fff fff fff fff',
        0x00000001,
        1, 2, 3,
        4, 5, 6,
        7, 8, 9,
        10, 11, 12,
        13, 14, 15,
        16, 17, 18)

    print("tx_payload -- {0:d} -- {1:s}".format(len(tx_payload), str(list(tx_payload))))
    tx_frame = bytes(Codec.encode(tx_payload))
    print("tx_frame -- {0:d} -- {1:s}".format(len(tx_frame), str(list(tx_frame))))

    # simulate we received 2 frames back to back
    c = Codec()
    for byte in (tx_frame + tx_frame):
        (rx_payload, rx_frame, frame_detected) = c.decode(byte)
        if frame_detected:
            if rx_payload == tx_payload:
                print("match payload -- {:s}".format(str(list(rx_payload))))
                print("match frame   -- {:s}".format(str(list(rx_frame))))
            else:
                print("fail payload -- {:s}".format(str(list(rx_payload))))
                print("fail frame   -- {:s}".format(str(list(rx_frame))))


if __name__ == '__main__':
    test()
