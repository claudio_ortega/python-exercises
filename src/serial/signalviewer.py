import time
import matplotlib
import matplotlib.pyplot
import matplotlib.animation
import queue
import threading
import collections
import serial
import struct
import platform
import socket
from datetime import datetime

from codec import Codec

# type definitions
Point_3D = collections.namedtuple("Point_3D", "x,y,z")


class TimeGraph3:
    """
        delta_t:  sampling time
        screen_t: time for one screen, total from left to right
    """

    def __init__(self, include_x: bool, include_y: bool, include_z: bool, axis, t_sampling, t_screen):

        self.include_x = include_x
        self.include_y = include_y
        self.include_z = include_z

        self.axis = axis
        self.t_sampling = t_sampling
        self.t_screen = t_screen
        self.t_current = 0
        self.last_sample_time = 0
        self.t_left = -1

        self.t_list = []
        self.x_list = []
        self.y_list = []
        self.z_list = []

        axis.grid(color="grey")
        axis.set_xlim(self.t_left, self.t_screen)
        axis.set_ylim(-1, 1)

        text_x1 = 0.85
        text_y1 = 0.85
        self.line_value_text_t = axis.text(text_x1, text_y1 - 0.0, 'n/a', transform=axis.transAxes, fontsize='medium',
                                           fontfamily='monospace')
        self.line_value_text_x = axis.text(text_x1, text_y1 - 0.1, 'n/a', transform=axis.transAxes, fontsize='medium',
                                           fontfamily='monospace')
        self.line_value_text_y = axis.text(text_x1, text_y1 - 0.2, 'n/a', transform=axis.transAxes, fontsize='medium',
                                           fontfamily='monospace')
        self.line_value_text_z = axis.text(text_x1, text_y1 - 0.3, 'n/a', transform=axis.transAxes, fontsize='medium',
                                           fontfamily='monospace')

        self.line_label_t = 'time'

        if self.include_x:
            self.line_label_x = 'x'
            self.style_x = 'r'
            self.line_x = axis.plot([], [], self.style_x, label=self.line_label_x)[0]

        if self.include_y:
            self.line_label_y = 'y'
            self.style_y = 'g'
            self.line_y = axis.plot([], [], self.style_y, label=self.line_label_y)[0]

        if self.include_z:
            self.line_label_z = 'z'
            self.style_z = 'b'
            self.line_z = axis.plot([], [], self.style_z, label=self.line_label_z)[0]

    def update_data(self, sample_time: int, point_3d: Point_3D):

        self.t_current = sample_time * self.t_sampling

        if self.t_left < 0 or \
                sample_time - self.last_sample_time > 10 or \
                self.t_current > (self.t_left + self.t_screen) or \
                self.t_current < self.t_left:
            self.t_left = self.t_current
            self.axis.set_xlim(self.t_left, self.t_left + self.t_screen)
            self.t_list = []
            self.x_list = []
            self.y_list = []
            self.z_list = []
            #print("reset, self.t_current:{}".format(self.t_current))

        self.last_sample_time = sample_time

        self.line_value_text_t.set_text('t={0:.3f} s={1}'.format(self.t_current, sample_time))
        self.t_list.append(self.t_current)

        if self.include_x:
            self.line_value_text_x.set_text('x={0:+.3f}'.format(point_3d.x))
            self.x_list.append(point_3d.x)
            self.line_x.set_xdata(self.t_list)
            self.line_x.set_ydata(self.x_list)

        if self.include_y:
            self.line_value_text_y.set_text('y={0:+.3f}'.format(point_3d.y))
            self.y_list.append(point_3d.y)
            self.line_y.set_xdata(self.t_list)
            self.line_y.set_ydata(self.y_list)

        if self.include_z:
            self.line_value_text_z.set_text('z={0:+.3f}'.format(point_3d.z))
            self.z_list.append(point_3d.z)
            self.line_z.set_xdata(self.t_list)
            self.line_z.set_ydata(self.z_list)

    def set_signals_lim(self, _min, _max):
        self.axis.set_ylim(_min, _max)


def update_data(frame, graph_a, graph_b, graph_c, graph_d, graph_e, graph_f: TimeGraph3, q: queue.Queue):
    # read out all content in the queue
    q_length = q.qsize()
    for c in range(q_length):
        sample = q.get(block=True, timeout=None)
        graph_a.update_data(sample.t, sample.channels[0])
        graph_b.update_data(sample.t, sample.channels[1])
        graph_c.update_data(sample.t, sample.channels[2])
        graph_d.update_data(sample.t, sample.channels[3])
        graph_e.update_data(sample.t, sample.channels[4])
        graph_f.update_data(sample.t, sample.channels[5])


"""
    One instance of Sample holds:
     - a list of Point_3D all corresponding to the same sampling time in non-dimensional sample units
     - the value for the sampling time in seconds
"""


class Sample:
    def __init__(self):
        self.t = 0
        self.channels = []  # Point_3D


def produce_sample(rcv_data) -> Sample:
    sample = Sample()
    # time
    sample.t = rcv_data[0]
    # acceleration
    sample.channels.append(Point_3D(x=rcv_data[1], y=rcv_data[2], z=rcv_data[3]))
    # angular velocity
    sample.channels.append(Point_3D(x=rcv_data[4], y=rcv_data[5], z=rcv_data[6]))
    # magnetic field
    sample.channels.append(Point_3D(x=rcv_data[7], y=rcv_data[8], z=rcv_data[9]))
    # pressure
    sample.channels.append(Point_3D(x=rcv_data[10], y=0, z=0))
    # temperature
    sample.channels.append(Point_3D(x=rcv_data[11], y=0, z=0))
    # altitude
    sample.channels.append(Point_3D(x=rcv_data[12], y=0, z=0))
    return sample


class LossChecker:
    def __init__(self):
        self.init = False
        self.first_t = 0
        self.last_t = 0
        self.sum_distance = 0
        self.sum_error = 0
        self.sum_no_error = 0

    def check(self, t: int):
        if not self.init:
            self.init = True
            self.first_t = t
            self.last_t = t
            self.sum_distance = 0
            self.sum_error = 0
            self.sum_no_error = 0
            return

        distance = t - self.last_t
        self.last_t = t

        # distance == 1 means OK
        if distance > 1:
            self.sum_distance = self.sum_distance + distance
            self.sum_error = self.sum_error + 1
        else:
            self.sum_no_error = self.sum_no_error + 1

        if distance > 1:
            print(
                "t:{}, distance:{}, sum_distance:{}, sum_error:{}, sum_no_error:{}, "
                "error_ratio1:{:.3e}, error_ratio2:{:.3e}, time:{}".format(
                    t,
                    distance,
                    self.sum_distance,
                    self.sum_error,
                    self.sum_no_error,
                    self.sum_distance / (self.last_t - self.first_t),
                    self.sum_error / (self.sum_error + self.sum_no_error),
                    datetime.now()))


def serial_rx_task(kill_switch: threading.Event, q: queue.Queue):
    serial_conn = serial.Serial()
    serial_conn.baudrate = 921600
    serial_conn.timeout = 0.5
    if platform.system() == "Linux":
        serial_conn.port = "/dev/ttyUSB0"
    else:
        serial_conn.port = "/dev/tty.usbserial-AO0099I5"
    serial_conn.open()
    c = Codec()
    loss_checker = LossChecker()
    while not kill_switch.is_set():
        try:
            rx_bytes = serial_conn.read(10)
            for rx_byte in rx_bytes:
                (payload, frame, ready) = c.decode(rx_byte)
                if ready:
                    rcv_data = struct.unpack('<I fff fff fff fff fff fff', payload)
                    sample = produce_sample(rcv_data)
                    if sample.t % 10000 == 0:
                        print("rcv_data.sample.t:{}".format(sample.t))
                    q.put(sample)
                    loss_checker.check(sample.t)
        except Exception as e:
            time.sleep(1)
            print("failed to connect: " + str(e))


def udp_rx_task(kill_switch: threading.Event, q: queue.Queue):
    udp_server_socket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
    udp_server_socket.bind(("192.168.2.67", 20001))
    c = Codec()
    loss_checker = LossChecker()
    while not kill_switch.is_set():
        bytes_address_pair = udp_server_socket.recvfrom(1024)
        for rx_byte in bytes_address_pair[0]:
            # print("address:{}, rx_byte:{}".format( str(bytes_address_pair[1]), rx_byte))
            (payload, frame, ready) = c.decode(rx_byte)
            if ready:
                rcv_data = struct.unpack('<I fff fff fff fff fff fff', payload)
                sample = produce_sample(rcv_data)
                q.put(sample)
                loss_checker.check(sample.t)


def test(use_uart: bool):
    thread_kill_switch = threading.Event()

    def on_close(_):
        print('quit')
        thread_kill_switch.set()

    fig = matplotlib.pyplot.figure(figsize=(15, 9))
    fig.canvas.mpl_connect('close_event', on_close)
    axes = fig.subplots(nrows=6, ncols=1, subplot_kw={'facecolor': "#d0d0d0"})

    screen_t = 5
    delta_t = 0.005

    # accel
    tg_a = TimeGraph3(True, True, True, axes[0], delta_t, screen_t)
    tg_a.set_signals_lim(-30, 30)

    # omega
    tg_b = TimeGraph3(True, True, True, axes[1], delta_t, screen_t)
    tg_b.set_signals_lim(-5, 5)

    # mag
    tg_c = TimeGraph3(True, True, True, axes[2], delta_t, screen_t)
    tg_c.set_signals_lim(-1000, 1000)

    # temperature
    tg_d = TimeGraph3(True, True, False, axes[3], delta_t, screen_t)
    tg_d.set_signals_lim(10.0, 30.0)

    # pressure
    tg_e = TimeGraph3(True, True, False, axes[4], delta_t, screen_t)
    tg_e.set_signals_lim(105000, 115000)

    # altitude
    tg_f = TimeGraph3(True, True, False, axes[5], delta_t, screen_t)
    tg_f.set_signals_lim(25610, 25620)

    q = queue.Queue()
    anim = matplotlib.animation.FuncAnimation(
        fig,
        update_data,
        fargs=(tg_a, tg_b, tg_c, tg_d, tg_e, tg_f, q),
        interval=20,
        frames=None,
        cache_frame_data=False,
    )  # in ms

    if use_uart:
        rx_thread = threading.Thread(target=serial_rx_task, args=(thread_kill_switch, q))
    else:
        rx_thread = threading.Thread(target=udp_rx_task, args=(thread_kill_switch, q))

    rx_thread.start()

    matplotlib.pyplot.show()


if __name__ == '__main__':
    test(use_uart=True)
