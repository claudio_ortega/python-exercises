import serial
import timeit
import datetime
import platform


def main():
    try:
        serial_conn = serial.Serial()
        if platform.system() == "Linux":
            serial_conn.port = '/dev/ttyUSB0'
        else:
            serial_conn.port = "/dev/tty.usbserial-AO0099I5"
        serial_conn.baudrate = 115200
        serial_conn.timeout = 0.5
        serial_conn.open()
        print("connected OK")

        from timeit import default_timer as timer

        start = timeit.default_timer()

        for i in range(10):
            print("sending i:{:d}".format(i))
            serial_conn.write(b'set_led on\n')
            serial_conn.write(b'set_led off\n')

        serial_conn.close()
        print("disconnected OK")

        end = timeit.default_timer()
        print("elapsed: {:}".format(datetime.timedelta(seconds=end-start)))

    except serial.SerialException as e:
        print("failed to connect: " + str(e))


if __name__ == '__main__':
    main()

