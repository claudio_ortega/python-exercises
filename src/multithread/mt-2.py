import threading


class Counter:

    def __init__(self):
        self.data: int = 0
        self.lock = threading.Lock()

    def increment(self):
        with self.lock:
            self.data = self.data + 1

    def decrement(self):
        with self.lock:
            self.data = self.data - 1

    def getdata(self) -> int:
        with self.lock:
            return self.data


def task_1(msg: str, range_: int, some: Counter):
    for i in range(range_):
        some.increment()
        print("{0:s} -- {1:d} -- {2:d}".format(msg, i, some.getdata()))


def task_2(msg: str, range_: int, some: Counter):
    for i in range(range_):
        some.decrement()
        print("{0:s} -- {1:d} -- {2:d}".format(msg, i, some.getdata()))


def main():
    something = Counter()

    t1 = threading.Thread(target=task_1, args=("task_1", 3, something))
    t2 = threading.Thread(target=task_2, args=("task_2", 3, something))

    t1.start()
    t2.start()

    t1.join()
    t2.join()

    print("{something:d}", format(something.getdata()))
    assert (something.getdata() == 0)


if __name__ == "__main__":
    main()

