import threading
import time


def task(tag: str, sleep_sec: int, some: threading.Semaphore):
    print(tag)
    time.sleep(sleep_sec)
    some.release()


if __name__ == "__main__":

    semaphore = threading.Semaphore(value=0)

    t1 = threading.Thread(target=task, args=("task_1", 5, semaphore))
    t2 = threading.Thread(target=task, args=("task_2", 10, semaphore))
    t1.start()
    t2.start()

    print("a1")
    semaphore.acquire()
    print("a2")
    semaphore.acquire()
    print("b")

    t1.join()
    t2.join()
    print("c")

