import threading
import time


def task_1(msg: str, count: int):
    for i in range(count):
        print("{0:s} -- {1:d} -- {2:d}".format(msg, i, threading.active_count()))
        time.sleep(1)


def task_2(msg: str, count: int):
    for i in range(count):
        print("{0:s} -- {1:d} -- {2:d}".format(msg, i, threading.active_count()))
        time.sleep(1)


def main():

    t1 = threading.Thread(target=task_1, args=("task_1_msg", 2))
    t2 = threading.Thread(target=task_2, args=("task_2_msg", 1))

    t1.start()
    t2.start()

    t1.join()
    t2.join()


if __name__ == "__main__":
    main()
