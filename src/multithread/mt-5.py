import threading
import queue
import collections


# new type definition
Point_3D = collections.namedtuple("Point_3D", "channel_name, x,y,z")


class Sample:
    def __init__(self):
        self.t = 0
        self.channels = []  # Point_3D

    def print(self, tag: str):
        for ch in self.channels:
            print("{:s} -- t:{:d}, channel_name:{:s}, x:{:d}, y:{:d}, z:{:d}".format(tag, self.t, ch.channel_name, ch.x, ch.y, ch.z))


def task_producer(q: queue.Queue, count: int, dt_msec: int):
    for i in range(count):
        sample = Sample()
        sample.t = i * dt_msec
        sample.channels.append(Point_3D(channel_name="channel-1", x=1*i, y=2*i, z=3*i))
        sample.channels.append(Point_3D(channel_name="channel-2", x=10*i, y=20*i, z=30*i))
        sample.print("producer")
        q.put(sample)


def task_consumer(q: queue.Queue, count: int):
    for c in range(count):
        sample = q.get(block=True, timeout=None)
        sample.print("consumer")


def main():

    print("start")

    q = queue.Queue()

    n = 3
    t_p = threading.Thread(target=task_producer, args=(q, n, 10))
    t_c = threading.Thread(target=task_consumer, args=(q, n))

    t_c.start()
    t_p.start()

    t_c.join()
    t_p.join()

    print("stop")


if __name__ == "__main__":
    main()
