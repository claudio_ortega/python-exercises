import queue
import threading
import time


def producer_task(q: queue.Queue):
    tx_data = 0
    while True:
        # simulate some process that takes time to produce each data
        tx_data += 1
        time.sleep(1.0)

        # send data
        q.put(tx_data)


def consumer_task(q: queue.Queue):
    while True:
        try_count = 0
        # read all data available
        while q.qsize() > 0:
            try_count += 1
            print("consumer - try_count: {:d}, q.qsize(): {:d}".format(try_count, q.qsize()))

            rx_data = q.get(block=True, timeout=None)
            print("consumer - rx_data:   {:d}".format(rx_data))

            # simulate some process that takes some time to process each received data
            time.sleep(2.0)


def main():
    q = queue.Queue()
    producer_thread = threading.Thread(target=producer_task, daemon=True, args=(q,))
    consumer_thread = threading.Thread(target=consumer_task, daemon=True, args=(q,))

    consumer_thread.start()
    producer_thread.start()
    print("threads started")

    consumer_thread.join()
    producer_thread.join()
    print("threads stopped")


if __name__ == "__main__":
    main()
