import threading

if __name__ == "__main__":

    semaphore = threading.Semaphore(value=0)

    def hello():
        print("hello, world")
        semaphore.release()

    t = threading.Timer(5.0, hello)

    print("a1")
    t.start()  # after 5 seconds, "hello, world" will be printed
    print("a2")
    semaphore.acquire()
    print("a3")



